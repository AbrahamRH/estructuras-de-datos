/* Copyright (C)
 * 2019 - AbrahamRH
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/**
 * @file List.hpp
 * @brief Fichero de cabecera de la estructura de datos de una lista
 * @author AbrahamRH
 * @version 1.1
 * @date 2019-12-05
 */

#include <iostream>

#ifndef LISTA
#define LISTA

template <class Item>
struct Node
{
	Item data;
	struct Node* next;
	struct Node* prev;
};

template <class Item>
class List
{
	private:
		size_t len;									//Tamaño de la lista
		struct Node<Item>* first;					//Apuntador al inicio de la lista
		struct Node<Item>* last;					//Apuntador al final de la lista
		struct Node<Item>* cursor;					//Apuntador al nodo en curso

		/* -------------------------------*/
		/**
		 * @brief Método para crear un nodo  de la lista
		 *
		 * @param _data Dato a insertar en el nodo
		 *
		 * @return Apuntador al nodo creado
		 */
		/* -------------------------------*/
		Node<Item>* newNode( Item _data );

	public:
		/* -------------------------------*/
		/**
		 * @brief Constructor de la clase Lst
		 */
		/* -------------------------------*/
		List();

		/* -------------------------------*/
		/**
		 * @brief Destrutor de la clae List
		 */
		/* -------------------------------*/
		~List();

		/* -------------------------------*/
		/**
		 * @brief Método para verificar si la lista esta vacia
		 *
		 * @return True: si esa vacia, False: en caso contrario
		 */
		/* -------------------------------*/
		bool isEmpty();

		/* -------------------------------*/
		/**
		 * @brief Metodo para insertar un nodo al inicio de la lista
		 *
		 * @param data Contenido del nodo a insertar
		 *
		 * @return True: Si se logro insertar, False: caso contrario
		 */
		/* -------------------------------*/
		bool insertFront( Item data );

		/* -------------------------------*/
		/**
		 * @brief Método para eliminar un elemento de la lista
		 *
		 * @param pos Posición del elemento a eliminar
		 */
		/* -------------------------------*/
		void remove( int pos );

		/* -------------------------------*/
		/**
		 * @brief Método para obtener la longitud de la lista
		 *
		 * @return Logitud de la lista
		 */
		/* -------------------------------*/
		size_t getLen();

		/* -------------------------------*/
		/**
		 * @brief Método para imprimir el contenido de una lista
		 */
		/* -------------------------------*/
		void print();

		/* -------------------------------*/
		/**
		 * @brief Método para mover el nodo Cursor al inicio de la lista
		 */
		/* -------------------------------*/
		void cursorFirst();

		/* -------------------------------*/
		/**
		 * @brief Método para mover el cursor al final de la lista
		 */
		/* -------------------------------*/
		void cursorLast();

		/* -------------------------------*/
		/**
		 * @brief Método para vaciar una lista
		 */
		/* -------------------------------*/
		void makeEmpty();

		/* -------------------------------*/
		/**
		 * @brief Método para insertar un nodo al final de la lista
		 *
		 * @param data Contenido del nodo a insertar
		 *
		 * @return True: si se inserto correctamente, False: en caso contrario.
		 */
		/* -------------------------------*/
		bool insertBack( Item data );

};

#endif
