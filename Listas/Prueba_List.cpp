/* Copyright (C)
 * 2019 - AbrahamRH
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/**
 * @file Prueba_List.cpp
 * @brief Driver program para la estructura List
 * @author AbrahamRH
 * @version 2.1
 * @date 2019-12-13
 */

#include <iostream>

#include "List.hpp"

int main()
{
	List<int> l;

	l.insertFront(5);

	l.insertFront(10);

	l.insertBack(4);

	l.print();

#if 0

	l.makeEmpty();

	l.print();

	std::cout << l.getLen() <<std::endl;

	if( l.isEmpty() == true )
		std::cout << "Esta vacio" <<std::endl;
	else
		std::cout << "Tiene contenido" <<std::endl;
#endif

	return 0;
}

