/* Copyright (C)
 * 2019 - AbrahamRH
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/**
 * @file List.cpp
 * @brief Archivo de implementación de los métodos de la Lista
 * @author AbrahamRH
 * @version 1.1
 * @date 2019-12-05
 */

#include "List.hpp"
#include <cassert>

template <class Item>
List<Item>::List()
{
	this->first  = nullptr;
	this->last   = nullptr;
	this->cursor = nullptr;
	this->len    = 0;
}

template <class Item>
List<Item>::~List()
{
	while( !this->isEmpty() ){
		Node<Item>* n = this->first->next;
		delete this->first;
		this->first = n;
		--this->len;
	}
	delete this;
}

template <class Item>
Node<Item>* List<Item>::newNode( Item _data )
{
	Node<Item>* n = new Node<Item>;
	if( n ){
		n->data = _data;
		n->next = nullptr;
		n->prev = nullptr;
	}
	return n;
}

template <class Item>
bool List<Item>::isEmpty()
{
	return this->first == nullptr;
}

template <class Item>
bool List<Item>::insertFront( Item data )
{
	bool done = false;

	Node<Item>* n = this->newNode( data );

	if( n ){
		done = true;
		if( this->isEmpty() ){
			this->first = this->last = n;
			this->len = 1;
		} else {
			n->next = this->first;
			this->first->prev = n;
			this->first = n;
			++this->len;
		}
	}

	return done;
}

template <class Item>
size_t List<Item>::getLen()
{
	return this->len;
}

template <class Item>
void List<Item>::print()
{
	this->cursorFirst();
	printf("\n");
	while( this->cursor != nullptr ){
		std::cout << this->cursor->data << "->";
		this->cursor = this->cursor->next;
	}
	std::cout << "Nil" << std::endl;
}

template <class Item>
void List<Item>::cursorFirst()
{
	this->cursor = this->first;
}

template <class Item>
void List<Item>::cursorLast()
{
	this->cursor = this->last;
}

template <class Item>
void List<Item>::makeEmpty()
{
	while( !this->isEmpty() ){
		Node<Item>* n = this->first->next;
		delete this->first;
		this->first = n;
		--this->len;
	}

	assert( this->len == 0 );

	this->first = this->last = this->cursor = nullptr;

}

template <class Item>
bool List<Item>::insertBack( Item data )
{
	bool done = false;

	Node<Item>* n = this->newNode( data );

	if( n ){
		done = true;
		if( this->isEmpty() ){
			this->first = this->last = n;
			this->len = 1;
		}else{
			this->last->next = n;
			n->prev = this->last;
			this->last = n;
			++this->len;
		}
	}
	return done;
}

