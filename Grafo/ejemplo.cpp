/* Copyright (C) 
 * 2020 - AbrahamRH
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file ejemplo.cpp
 * @brief Driver program para la estructura grafo
 * @author AbrahamRH
 * @version 2.0
 * @date 2020-04-11
 */

#include "grafo.hpp"

//Para un uso más facil de las estructuras
using namespace GraphDS; 

int main()
{

/* 	//Forma 1 de usar la estructura
	GraphDS::Graph g;


	g.add_vertex(GraphDS::Vertex("A"));
	g.add_vertex(GraphDS::Vertex("B"));

	g.add_edge("A","B");

	printf("\n-----Grafo g-----\n");
	g.print();
	g.BFS(g.get_vertex("A"));

	//Forma 2 (Solo si se incluye el namespace)
	Graph h;

	h.add_vertex(Vertex("C"));
	h.add_vertex(Vertex("D"));

	h.add_edge("C","D");

	printf("\n-----Grafo h-----\n");
	h.print();
	h.BFS(h.get_vertex("C"));
 */

	Graph g;

	g.add_vertex(Vertex("10"));
	g.add_vertex(Vertex("20"));
	g.add_vertex(Vertex("80"));
	g.add_vertex(Vertex("50"));
	g.add_vertex(Vertex("90"));
	g.add_vertex(Vertex("40"));
	g.add_vertex(Vertex("60"));
	g.add_vertex(Vertex("30"));
	g.add_vertex(Vertex("70"));

	g.add_edge("10","20");
	g.add_edge("10","40");
	g.add_edge("10","80");
	g.add_edge("20","60");
	g.add_edge("40","90");
	g.add_edge("80","50");
	g.add_edge("60","30");
	g.add_edge("30","70");

	g.print();
	
	g.BFS(g.get_vertex("10"));


	return 0;

}
